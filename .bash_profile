# .bash_profile
# vim: nu: tabstop=8: softtabstop=8: shiftwidth=8

# Set PATH
#=================================================================================================#
unset PATH
PATH+="$HOME/bin:"
PATH+="/usr/local/bin:"
PATH+="/usr/local/sbin:"
PATH+="/opt/local/bin:"
PATH+="/opt/local/sbin:"
PATH+="/usr/bin:"
PATH+="/usr/sbin:"
PATH+="/bin:"
PATH+="/sbin:"
export PATH

# Execute user defined aliases and functions
#=================================================================================================#
. ~/.bashrc &>/dev/null
