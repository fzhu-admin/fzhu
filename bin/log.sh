#!/bin/bash
# vim: nu: tabstop=4: softtabstop=4: shiftwidth=4
#=================================================================================================#
# Description: log.sh appends messages to log files. It is intended for use in other scripts.
#=================================================================================================#
while getopts ":hd:f:m:" OPT; do
	case $OPT in
		d)
			declare -r LOGDIR=$OPTARG
			;;
		f)
			declare -r LOGFILE=$OPTARG
			;;
		m)
			declare -r LOGMESG=$OPTARG
			;;
	esac
done

#=================================================================================================#
# Limit PEBKAC by setting default values
#=================================================================================================#
[ -z "$LOGDIR" ] && LOGDIR=$HOME/log
[ -z "$LOGFILE" ] && LOGFILE=general
[ -z "$LOGMESG" ] && LOGMESG="[?] Dingus! You didn't provide a log message..."

#=================================================================================================#
function log {
#
# ARGUMENTS:	Message string
# INHERITANCE:	Global
# RETURNS:		Nothing
#
#=================================================================================================#
	#
	# If log directory doesn't exist, make it.
	[ ! -d $LOGDIR ] && mkdir -p $LOGDIR
	#
	# Append message passed in as argument to log file
	echo $(date) $LOGMESG >> $LOGDIR/$LOGFILE
}

#=================================================================================================#
log
#=================================================================================================#
