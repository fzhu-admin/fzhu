#!/bin/bash
# vim: nu: tabstop=4: softtabstop=4: shiftwidth=4
#=================================================================================================#
declare -r USAGE="Usage: fanify.sh [-c] [-i] [-v] [-V]

Description: fanify.sh synchronizes files in the home directory
of the fzhu user with files track in a remote git repository.

Optional Flags:
	-h,	print this message and exit.
	-c,	force overwrite crontab entries
	-i,	force overwrite .gitinclude entries
	-m,	force overwrite .modinclude entries
	-v,	run this script in verbose mode.
	-V,	like verbose but without color.
"

#=================================================================================================#
# Define origin repo info
#=================================================================================================#
DOMAIN=bitbucket.org
OWNER=fzhu-admin
declare -r REPO=fzhu.git
declare -r PULL_URL=https://$DOMAIN/$OWNER/$REPO
declare -r PUSH_URL=git@$DOMAIN:$OWNER/$REPO
declare -r PUSH_KEY=$HOME/.ssh/git_rsa
#
# Unset unneeded values
unset DOMAIN OWNER

#=================================================================================================#
# Define local directory aliases
#=================================================================================================#
declare -r CLONE=.fzhu
declare -r SKELETONS=$HOME/.skeletons
declare -r MODULES=$HOME/bin/fanify

#=================================================================================================#
# Define default options
#=================================================================================================#
#
# Colors used
GRN="\033[00;32m" WHT="\033[00m"
#
# Verbosity
Q=-q S=-s V=
#
# $Q and $S, are used with commands that accept -q or -s flags for no output and $V is used with
# commands that accept -v for verbose output. By default, $Q and $S are set because 90% of the
# time this script is executed, it will be from cron. Flagging -v unsets $Q and $S and sets $V.

#=================================================================================================#
# Define auto-detect options
#=================================================================================================#
#
# If crontab is empty, make crontab entries even if -c has not been flagged.
if [ -z "$(crontab -l)" ]; then
	declare -r MK_CRONTAB=yes
#
# If crontab is not empty but $CRONJOB is not defined, backup existing entries, then nuke and pave
# crontab entries.
elif [ -z "$(crontab -l | grep CRONJOB )" ]; then
	declare -r MK_CRONTAB=yes
	declare -r MK_CRONTAB_BAK=yes
fi
#
# $CRONJOB is an environment variable set within crontab the first time this script is executed.
# This variable should exist. If it doesn't, we should nuke and pave because cron's environment is
# not consistent with what we expect.
#
# If .gitignore does not exist, make .gitignore entries even if -i has not been flagged.
[ ! -f $HOME/.gitinclude ] && declare -r MK_GITINCLUDE=yes
#
# If .modignore does not exist, make .modignore entries even if -m has not been flagged.
[ ! -f $HOME/.modinclude ] && declare -r MK_MODINCLUDE=yes

#=================================================================================================#
# Define optional flags
#=================================================================================================#
while getopts ":hcimvV" OPT; do
	case $OPT in
		h)
			printf "$USAGE"
			exit 0
			;;
		#
		# Past this point, options are sorted alphabetically
		#
		c)
			#
			# If crontab isn't empty, backup existing entries before overwrite
			[ -n "$(crontab -l)" ] && declare -r MK_CRONTAB_BAK=yes
			[ -z "$MK_CRONTAB" ] && declare -r MK_CRONTAB=yes
			;;
		i)
			#
			# If .gitinclude already exists, backup existing entries before overwrite
			[ -f $HOME/.gitinclude ] && declare -r MK_GITINCLUDE_BAK=yes
			[ -z "$MK_GITINCLUDE" ] && declare -r MK_GITINCLUDE=yes
			;;
		m)
			#
			# If .gitinclude already exists, backup existing entries before overwrite
			[ -f $HOME/.modinclude ] && declare -r MK_MODINCLUDE_BAK=yes
			[ -z "$MK_MODINCLUDE" ] && declare -r MK_MODINCLUDE=yes
			;;
		v)
			#
			# Set verbose, unset quiet and silent
			declare -r V=-v
			unset Q
			unset S
			;;
		V)
			#
			# Unset colors
			unset GRN
			unset WHT
			;;
	esac
done

#=================================================================================================#
function main {
#=================================================================================================#
	#
	# Mark start of execution
	[ -z "$CRONJOB" ] && declare -r WHOM=$USER || declare -r WHOM="cron daemon"
	$HOME/bin/log.sh -m "[-] $HOME/bin/fanify.sh execution started by $WHOM"
	#
	# Change to $HOME before proceeding. 
	cd $HOME
	#
	# Clone repo
	clone_fzhu
	#
	# Copy default .gitinclude if $MK_GITINCLUDE not empty string
	[ -n "$MK_GITINCLUDE_BAK" ] && cp $HOME/.gitinclude $HOME/.gitinclude.bak
	[ -n "$MK_GITINCLUDE" ] && cp $SKELETONS/gitinclude $HOME/.gitinclude
	#
	# Nuke directories listed in .gitinclude and overwrite files in $HOME
	rm -rf $(cat $HOME/.gitinclude)
	rsync_files
	#
	# Our intention here is to enforce consistency and foster the following mentality: "If you
	# don't commit it, you will lose it."
	#
	# Redefine pushurl to use ssh instead of https
	git remote set-url --push origin $PUSH_URL
	#
	# If push key exists, make sure ssh agent knows about it
	[ -f $PUSH_KEY ] && timeout 1m ssh-add $PUSH_KEY 2>/dev/null
	#
	# A timeout is used in case ssh agent prompts for a secret phrase. This is an edge case.
	#
	# Copy default .modinclude if $MK_MODINCLUDE not empty string
	[ -n "$MK_MODINCLUDE_BAK" ] && cp $HOME/.modinclude $HOME/.modinclude.bak
	[ -n "$MK_MODINCLUDE" ] && cp $SKELETONS/modinclude $HOME/.modinclude
	#
	# Execute .modinclude
	. $HOME/.modinclude
	#
	# Refresh .gitignore
	make_ignore_file $(cat $HOME/.gitinclude)
	#
	# If -c is flagged or crontab is empty, make default entries.
	[ -n "$MK_CRONTAB_BAK" ] && crontab -l > .crontab.bak
	[ -n "$MK_CRONTAB" ] && make_crontab_entries
	#
	# Mark completion and exit
	$HOME/bin/log.sh -m "[0] $HOME/bin/fanify.sh execution complete"
}

#=================================================================================================#
function clone_fzhu {
#
# ARGS:		None
# SCOPE:	Global
# RETURNS:	Nothing
#
# This function clones fzhu.git from a remote repository into a temporary directory under $HOME.
# A temporary directory is used because clone fails when the destination directory we've specified
# isn't empty. This will be the case most of the time this script runs.
#
#=================================================================================================#
	#
	# Show execution stage if verbose
	[ -n "$V" ] && printf "\n${GRN}Cloning $REPO from $PULL_URL${WHT}\n"
	#
	# Clone remote repo or document exit code if clone fails
	git clone $Q $PULL_URL $HOME/$CLONE || local EC=$?
	#
	# Log premature exit and cleanup temporary files if $EC isn't an empty string
	if [ -n "$EC" ]; then
		log.sh -m "[$EC] Failed to clone $REPO into $HOME/$CLONE from $PULL_URL"
		rm -rf $HOME/$CLONE
		exit $EC
	fi
}

#=================================================================================================#
function make_crontab_entries {
#
# ARGS:		None
# SCOPE:	Global
# RETURNS:	Nothing
#
# This function generates crontab entries that will run this script on a daily basis.
#
#=================================================================================================#
	#
	# Nuke and pave crontab entries
	cat $SKELETONS/crontab | sed "s|PATH=|PATH=$PATH|" | crontab - || local EC=$?
	#
	# Log premature exit if $EC isn't an empty string
	if [ -n "$EC" ]; then
		log.sh -m "[$EC] Failed to write crontab entries"
		exit $EC
	fi
	#
	# Print execution stage and crontab entries if verbose
	if [ -n "$V" ]; then
		printf "\n${GRN}Generated crontab entries${WHT}\n"
		crontab -l
	fi
}

#=================================================================================================#
function make_ignore_file {
#
# ARGS:		Directories
# SCOPE:	Global
# RETURNS:	Nothing
#
# This function uses egrep -v to generate a .gitignore file for $HOME since not everything in $HOME
# should be tracked in git. It ensures that neither files tracked in git nor directories passed in
# as arguments will appear in .gitignore.
#
#=================================================================================================#
	#
	# Identify files tracked in git
	TRACKED_FILES=($(git ls-files))
	#
	# Elements in $TRACKED_FILES will be combined to form an exclude expression in the
	# following format:
	#
	#	"foo|bar|oof|rab"
	#
	# The nested loops below will determine if files tracked in git live in directories passed
	# into this function and remove them from $TRACKED_FILES.
	for DIR in $*; do
		for FILE in ${TRACKED_FILES[*]}; do
			if [ $(expr "$FILE" : "$DIR/.*") != 0 ]; then
				TRACKED_FILES=(${TRACKED_FILES[*]/$FILE})
			fi
		done
		EXCLUDE+="|$DIR"
	done
	#
	# The idea here is that since we're going to track everything in the directories passed
	# into this function, specific files from these directories shouldn't appear in our exlude
	# statement. Furthermore, cleaning up $TRACKED_FILES as we go means less looping for each
	# successive directory.
	#
	# Append anything that remains in $TRACKED_FILES to our exclude expression.
	for FILE in ${TRACKED_FILES[*]}; do
		EXCLUDE+="|$FILE"
	done
	#
	# Write .gitignore file
	[ -n "${EXCLUDE[*]}" ] && ls -1A | egrep -v "${EXCLUDE:1}" > $HOME/.gitignore
	#
	# Print execution stage and .gitignore if verbose
	if [ -n "$V" ]; then
		printf "\n${GRN}Refreshed .gitignore in $HOME${WHT}\n"
		cat $HOME/.gitignore
	fi
}

#=================================================================================================#
function rsync_files {
#
# ARGS:		None
# SCOPE:	Global
# RETURNS:	Nothing
#
# This function is a wrapper for rsync. Rsync is used to copy files from the temporary clone of
# fzhu.git to the $HOME.
#
#=================================================================================================#
	#
	# Nuke existing repo in $HOME
	rm -rf $HOME/.git
	#
	# Print execution stage and set itemize option if verbose
	if [ -n "$V" ]; then
		printf "\n${GRN}Copying files from $HOME/$CLONE to $HOME${WHT}\n"
		I=--itemize-changes
	fi
	#
	# Rsync files into $HOME
	rsync $I -r $HOME/$CLONE/ $HOME/ || local EC=$?
	#
	# Log premature exit and cleanup if $EC isn't an empty string
	if [ -n "$EC" ]; then
		log -m "[$EC] Failed to rsync files from $HOME/$CLONE to $HOME"
		rm -rf $HOME/$CLONE
		exit $EC
	fi
	#
	# Nuke temporary directory
	rm -rf $HOME/$CLONE 
}

#=================================================================================================#
main
#=================================================================================================#
