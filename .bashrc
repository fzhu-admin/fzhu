# .bashrc
# vim: nu: tabstop=8: softtabstop=8: shiftwidth=8

# Get global definitions
#=================================================================================================#
. /etc/bashrc &>/dev/null

# Get additional definitions
#=================================================================================================#
. ~/.git_prompt.sh &>/dev/null && GIT_PS1_SHOWSTATUSSTRING="on"

# Define ls aliases
#=================================================================================================#
alias ll='ls -lGFh'				# Long List
alias lls='ls -lGFhSr'				# Long List by size, biggest last
alias llt='ls -lGFhtr'				# Long List by date, most recent last
alias llc='ls -lGFhtcr'				# Long List by change time, most recent last
alias llu='ls -lGFhtur'				# Long List by access time, most recent last
alias la='ls -lGFhA'				# List almost all
alias las='ls -lGFhSrA'				# List almost all by size, biggest last
alias lat='ls -lGFhtrA'				# List almost all by date, most recent last
alias lac='ls -lGFhtcrA'			# List almost all by change time, most recent last
alias lau='ls -lGFhturA'			# List almost all by access time, most recent last

# Define miscellaneous aliases
#=================================================================================================#
alias mkpass='openssl rand -base64 9'			# Pseudo-random base64 encoded bytes
alias sconsole='screen /dev/cu.usbserial 9600 cs8'	# Serial console

# Configure prompt string
#=================================================================================================#
function generate_ps1 {

	# Store previous exit code. This has to come first.
	local EC="$?"

	# Define colors used
	local RED="\[\033[00;31m\]"
	local GRN="\[\033[00;32m\]"
	local CYN="\[\033[00;36m\]"
	local MAG="\[\033[00;35m\]"
	local WHT="\[\033[00m\]"

	# Colorize previous exit code
	if [ "$EC" != '0' ]; then
		EC="$RED$EC$WHT"
	else
		EC="$GRN$EC$WHT"
	fi  

	# Colorize current path
	local CP="$MAG\w$WHT"

	# Set git status configs
	GIT_PS1_SHOWUPSTREAM="git verbose"
	GIT_PS1_SHOWDIRTYSTATE="on"
	GIT_PS1_SHOWSTASHSTATE="on"
	GIT_PS1_SHOWCOLORHINTS="on"

	# Get git status for current path if git_prompt.sh has been executed
	if [ -n $GIT_PS1_SHOWSTATUSSTRING ]; then
		local GS="$(__git_ps1 "[%s]")"
	fi

	# Get timestamp
	local TS="$MAG\t$WHT"

	# Get user and hostname
	local UH="$CYN\u@\h$WHT"

	# Set PS1 if PS1 is not an empty string.
	if [ -n "$PS1" ]; then
		PS1="$TS[$UH]$GS:$CP\n[$EC]\\$ "
	fi  
}

# Refresh PS1 each time a prompt is returned
PROMPT_COMMAND=generate_ps1
